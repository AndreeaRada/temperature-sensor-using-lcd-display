#include <OneWire.h>
#include <DallasTemperature.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2); 

#define ONE_WIRE_BUS 2 //pin for sensor

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void setup(void)
{
  

  Serial.begin(9600);

  Serial.println("Dallas Temperature IC Control Library Demo");

  sensors.begin();

  lcd.init();
  
  lcd.backlight();

  lcd.setCursor(0, 0);

  lcd.print("DS18B20 TEST!");

  lcd.setCursor(0, 1);

  lcd.print("by miliohm.com");

  delay(2000);

  lcd.clear();

}
void loop(void)
{
 
  Serial.print("Requesting temperatures...");

  sensors.requestTemperatures(); 

  Serial.println("DONE");
  
  float tempC = sensors.getTempCByIndex(0);

  if (tempC != DEVICE_DISCONNECTED_C)
  {
    Serial.print("Temperature for the device 1 (index 0) is: ");

    Serial.println(tempC);

    lcd.setCursor(0, 0);

    lcd.print("Temperature:");

    lcd.setCursor(0, 1);

    lcd.print(tempC);

    lcd.print((char)223);

    lcd.print("C");

    lcd.print(" | ");

    lcd.print(DallasTemperature::toFahrenheit(tempC));

    lcd.print(" F");

  }

  else
  {
    Serial.println("Error: Could not read temperature data");

  }
}
