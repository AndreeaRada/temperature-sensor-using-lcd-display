**Temperature sensor using LCD**

This device can measure the temperature in the room, of a human person, a glass of water, etc.

**Components:**
- Arduino UNO
- USB 2.0 cable type A/B
- LCD I2C
- Temperature Sensor DS18B20
- 4.7kΩ resistor
- Breadboard
- Jumper Wires


**Connections:**
ARDUINO 	DS18B20	 RESISTOR	 LCD
VCC	        VCC	      VCC	     VCC
GND	        GND		             GND
2	        Data	  Data	
A4			                     SDA
A5			                     SCL

**CODE:**

Import the libraries DallasTemperature and OneWire from Manage Libraries.

Upload the code from temperaturesensor.ino


